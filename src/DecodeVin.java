import com.sun.xml.internal.ws.util.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.servlet.ServletContext;

import org.apache.http.client.utils.URIBuilder;

import java.util.HashMap;
import java.util.Iterator;

public class DecodeVin {

    public static String decode(String carJsonFromService, ServletContext context) throws Exception {
        String result = "";
        HashMap resultMap = new HashMap(); //result Map type
        String yearUrlVar = "";
        //JSON dictionaries for matching car data from external web service and our web site data
        JSONObject modelMatchJSON = (JSONObject) context.getAttribute("modelMatchJSON");
        JSONObject makeMatchJSON = (JSONObject) context.getAttribute("makeMatchJSON");
        JSONObject matchYearsJSON = (JSONObject) context.getAttribute("matchYearsJSON");
        JSONObject matchEngineJSON = (JSONObject) context.getAttribute("matchEngineJSON");
        JSONObject fsPageJSON = (JSONObject) context.getAttribute("fsPageJSON");

        //tools for parsing JSON car data string from external web service
        Object obj = new JSONParser().parse(carJsonFromService);
        JSONObject jsonObject = (JSONObject) obj;
        JSONObject modelJson = new JSONObject();
        //json object with car info returned from ext service
        JSONArray carData = (JSONArray) jsonObject.get("Results");
        if (carData == null) {
            return jsonObject.toString();
        }
        // take global variable from contest
        //String targetWebSite = (String) context.getAttribute("targetWebSite");
        //build url for search redirection on target web site
        URIBuilder builder = new URIBuilder();
        //builder.setScheme("http");
        //builder.setHost(targetWebSite);

        //loop trough all returned from vin service array elements
        Iterator<JSONObject> iterator = carData.iterator();
        while (iterator.hasNext()) {
            jsonObject = (JSONObject) iterator.next();
            String variable = (String) jsonObject.get("Variable");
            String variableId = String.valueOf(jsonObject.get("VariableId"));
            String value = (String) jsonObject.get("Value");
            if (variable.equals("Model Year") && variableId.equals("29")) {
                resultMap.put("Year", value);
                //if year found in modelMatch.json then add it in url
                if (matchYearsJSON.get(value) != null) {
                    builder.addParameter("year", matchYearsJSON.get(value).toString());
                }
            }
            if (variable.equals("Make") && variableId.equals("26")) {
                resultMap.put("Make", value.toUpperCase());
            }
            if (variable.equals("Model") && variableId.equals("28")) {
                resultMap.put("Model", value);
                modelJson = (JSONObject) modelMatchJSON.get(value.toUpperCase());
                //if model found in modelMatch.json then add it in url
                if (modelJson != null) {
                    builder.addParameter("model", modelJson.get("model").toString());
                }
            }

            if (variable.equals("Displacement (L)") && variableId.equals("13")) {
                resultMap.put("Engine", value);
                if (matchEngineJSON.get(value) != null) {
                    builder.addParameter("enginesize", matchEngineJSON.get(value).toString());
                }
            }

        }
        resultMap.put("URL", "");
        //search right webpage by Model from webservice
        if (modelJson != null) {
            String makeNumber = modelJson.get("make").toString();
            //take html page by make number
            if (fsPageJSON.get(makeNumber) != null) {
                builder.setPath("/" + fsPageJSON.get(makeNumber).toString());
                resultMap.put("URL", builder.toString());
            } else {
                resultMap.put("URL", "error in modelMatch.json and fsPage.json);");
            }
        }/* else //search right webpage by make from webservice
        {
            if (resultMap.get("Make") != null) {
                String make = resultMap.get("Make").toString();
                JSONObject makeJson = (JSONObject) makeMatchJSON.get(make);
                if (makeJson != null) {
                    builder.setPath("/" + makeJson.get("fspage").toString());
                    resultMap.put("URL", builder.toString());
                }
            }
        }
        */
        jsonObject = new JSONObject(resultMap);
        result = jsonObject.toJSONString();
        return result;
    }
    public static String toTitleCase(String input) {
        input = input.toLowerCase();
        char c =  input.charAt(0);
        String s = new String("" + c);
        String f = s.toUpperCase();
        return f + input.substring(1);
    }
}
/*
public class CrunchifyJSONReadFromFile {

    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        JSONParser parser = new JSONParser();

        try {

            Object obj = parser.parse(new FileReader(
                    "/Users/<username>/Documents/file1.txt"));

            JSONObject jsonObject = (JSONObject) obj;

            String name = (String) jsonObject.get("Name");
            String author = (String) jsonObject.get("Author");
            JSONArray companyList = (JSONArray) jsonObject.get("Company List");

            System.out.println("Name: " + name);
            System.out.println("Author: " + author);
            System.out.println("\nCompany List:");
            Iterator<String> iterator = companyList.iterator();
            while (iterator.hasNext()) {
                System.out.println(iterator.next());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    */