/*
Reading config file
 */

import org.json.simple.JSONObject;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

@WebListener
public class ApplicationStartUpListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent event) {
        System.out.println("---- initialize servlet context -----");
        //put properties to server context. Properties become global
        ServletContext context = event.getServletContext();
        Properties prop = new Properties();
        try {
            //read config file and save to server context
            InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("config.txt");
            prop.load(inputStream);
            Enumeration e = prop.propertyNames();
            while (e.hasMoreElements()) {
                String key = (String) e.nextElement();
                context.setAttribute(key, prop.getProperty(key));
            }
        } catch (Exception e) {
            context.setAttribute("Error", e);
        }
        //prop.getProperty("modelMatchDir")
        try {
            if (prop != null) {
                JSONObject modelMatchJSON = FileTool.JSONfromFile(prop.getProperty("modelMatchDir"));
                context.setAttribute("modelMatchJSON", modelMatchJSON);
                JSONObject makeMatchJSON = FileTool.JSONfromFile(prop.getProperty("makeMatchDir"));
                context.setAttribute("makeMatchJSON", makeMatchJSON);
                JSONObject matchYearsJSON = FileTool.JSONfromFile(prop.getProperty("matchYearsDir"));
                context.setAttribute("matchYearsJSON", matchYearsJSON);
                JSONObject matchEngineJSON = FileTool.JSONfromFile(prop.getProperty("matchEngineDir"));
                context.setAttribute("matchEngineJSON", matchEngineJSON);
                JSONObject fsPageJSON = FileTool.JSONfromFile(prop.getProperty("fsPageDir"));
                context.setAttribute("fsPageJSON", fsPageJSON);
            }
        } catch (Exception e) {
            context.setAttribute("Error", e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        System.out.println("---- destroying servlet context -----");
        // clean up resources
        ServletContext context = event.getServletContext();
        context.removeAttribute("tempDir");
    }
}
