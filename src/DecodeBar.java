import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.ServletContext;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

// http://mvnrepository.com/artifact/org.glassfish.jersey.media/jersey-media-multipart/2.17
// http://mvnrepository.com/artifact/org.jvnet.mimepull/mimepull/1.9.7
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

@Path("/file")
public class DecodeBar {
    @Context
    private ServletContext context;

    @POST
    @Path("/upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadFile(
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail) {
        //get stored attribute
        String dir = (String) context.getAttribute("tempDir");
        String uploadedFileLocation = FileTool.saveFile(uploadedInputStream, fileDetail.getFileName(), dir);
        if (uploadedFileLocation.isEmpty()) {
            return Response.status(200).entity("Can't create temporery image file").build();
        }
        ;
        String vin = sample.bardecode(uploadedFileLocation);
        vin = correction(vin);
        FileTool.delTempFile(uploadedFileLocation);
        try {
            //try deciper vin
            String car = "";
            if (vin.length() > 18) {
                //barcode recognition fail
                car = "{\"error\":\"" + vin + "\"}";
            } else {
                String decodedVin = HttpRequest.sendGet(vin);
                car = DecodeVin.decode(decodedVin, context);
            }
            //return Response.status(200).entity(car).build();
            return Response.ok(car, MediaType.APPLICATION_JSON).build();
        } catch (Exception e) {
            e.printStackTrace();
            //convert Stack Trace to string
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            String sStackTrace = sw.toString(); // stack trace as a string

            return Response.status(500).entity(sStackTrace).build();
        }
        //vin = "{\"vin\":\""+correction(vin)+"\"}";
        //return Response.ok(vin, MediaType.APPLICATION_JSON).build();
        //return Response.status(200).entity(vin).build();

    }

    private String correction(String vin) {
        // remove lead "I"
        if (vin.length() > 17) {
            if (vin.substring(0, 1).equals("I")) {
                vin = vin.substring(1, vin.length());
            }
        }
        //when in demowersion barcode library post ? insted of 3 last vin# symbols
        // change it to *
        if (vin.substring(14, 17).equals("???")) {
            vin = vin.substring(0, 14) + "*";
        }

        return vin;
    }

}



