/* html page for testing */
import org.json.simple.JSONObject;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import java.io.StringWriter;
import java.io.PrintWriter;


@Path("/index")
public class BarCodeIndex {
    @Context
    private ServletContext context;

        @GET
        // The Java method will produce content identified by the MIME Media type "text/plain"
        @Produces("text/html")
        public String getHtml() {
            Exception e = (Exception)context.getAttribute("Error");
            String str ="";
            if (e !=null ){
                str = exceptionToStr(e);
            }
            // Return some cliched textual content
            return "<html>\n" +
                    "<body>\n" +
                    "\t<h1>Vin# reader</h1>\n" +
                    "\n" +str+
                    "\t<form action=\"file/upload\" method=\"post\" enctype=\"multipart/form-data\">\n" +
                    "\n" +
                    "\t   <p>\n" +
                    "\t\tSelect a file : <input type=\"file\" name=\"file\" size=\"45\" />\n" +
                    "\t   </p>\n" +
                    "\n" +
                    "\t   <input type=\"submit\" value=\"Upload It\" />\n" +
                    "\t</form>\n" +
                    "\n" +
                    "</body>\n" +
                    "</html>";
        }

        private String exceptionToStr(Exception e){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            String sStackTrace = sw.toString(); // stack trace as a string
            return sStackTrace;
        }
}
