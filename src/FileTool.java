/* work with temporary image file and other file system related functions*/

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public class FileTool {
    //here wil be data from config file
    static Properties  config;
    //read config file
/*
    public static Properties readProperty() {
        String appConfigPath = "config.txt";
        Properties appProps = new Properties();
        try {
            appProps.load(new FileInputStream(appConfigPath));
        } catch (Exception e) {
            e.printStackTrace();
        }
        config = appProps;
        return appProps;
    }
*/
    //create temp file
    public static String saveFile(InputStream uploadedInputStream, String srcFileName, String dir) {
        Path tempDir = Paths.get(dir);
        File fileName;
        try {
            String srcFileNameExt = getFileExtension(srcFileName);
            fileName = File.createTempFile("bar", "." + srcFileNameExt, tempDir.toFile());
            fileName.deleteOnExit();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
        // save it
        writeToFile(uploadedInputStream, fileName);
        return fileName.getAbsolutePath();
    }

    public static void delTempFile(String file) {
        try {
            Files.deleteIfExists(Paths.get(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // save uploaded file to temp file
    private static void writeToFile(InputStream uploadedInputStream,
                                    File uploadedFileLocation) {
        try {
            int read = 0;
            byte[] bytes = new byte[1024];
            OutputStream out = new FileOutputStream(uploadedFileLocation, true);
            while ((read = uploadedInputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    // getting file extension from filename
    private static String getFileExtension(String fileName) {
        String extension = "";
        int i = fileName.lastIndexOf('.');
        int p = Math.max(fileName.lastIndexOf('/'), fileName.lastIndexOf('\\'));
        if (i > p) {
            extension = fileName.substring(i + 1);
        }
        return extension;
    }

    // parse file.json to JSON object
    public static JSONObject JSONfromFile(String file) throws Exception {

        //file = "C:\\Users\\FS1 Web User\\Documents\\barcoder\\src\\json\\modelMatch.json";
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(new FileReader(file));
        JSONObject jsonObject = (JSONObject) obj;
        return jsonObject;
    }

}
