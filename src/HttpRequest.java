import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;


public class HttpRequest {

    private static final String USER_AGENT = "Mozilla/5.0";

    // HTTP GET request
    public static String sendGet(String vin) throws Exception {
        //String vin ="1g1yy25r695700001";
        //if vin is not correct return an error
        if (vin.length() > 18) {
            return "{\"error\":\"" + vin + "\"}";
        }

        String url = "https://vpic.nhtsa.dot.gov/api/vehicles/DecodeVin/" + vin + "?format=json";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return response.toString();

    }
}