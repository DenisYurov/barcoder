import Softek.* ;

public class sample {
	public static String bardecode(String file) {
		int i ;
		int n ;
		int p;
		int errno ;
		int winerrno ;
		String s ;
		java.awt.Rectangle rect;

		/* Create Barcode class */
		Softek.Barcode barcode = new Softek.Barcode();

		barcode.LicenseKey = "8J7Q5-7U6FP-4IT55-I5Q54-7744K";

		/* Set some properties */
		barcode.ReadCode39 = 1 ;
		barcode.MultipleRead = 0 ;


		//java.awt.Rectangle r = new java.awt.Rectangle(1000, 500, 500, 300);
		//barcode.SetScanRect(r, 0);
		/* Scan Image */

		n = barcode.ScanBarCode(file) ;
		String res ="";
		if (n < 0)
		{
			errno = barcode.GetLastError() ;
			winerrno = barcode.GetLastWinError();
			res ="ScanBarcode returned " + n + "\n"+"Last Softek Error Number = " + errno + "\n"+"Last Windows Error Number = " + winerrno + "\n";

			System.out.print("ScanBarcode returned " + n + "\n");
			System.out.print("Last Softek Error Number = " + errno + "\n");
			System.out.print("Last Windows Error Number = " + winerrno + "\n");

			return res;
		}
			
		System.out.print("Found " + n + " barcodes\n") ;

		for (i = 0; i < n; i++)
		{
			s = barcode.GetBarString(i + 1) ;
			p = barcode.GetBarStringPage(i + 1);
			rect = barcode.GetBarStringRect(i + 1);
			System.out.print(s) ;
			res = res + s;
			System.out.print("\t\t(") ;
			s = barcode.GetBarStringType(i + 1) ;
			System.out.print(s) ;
			System.out.print(", Page ");
			System.out.print(p);
			System.out.print(", Pos ");
			System.out.print(rect.x);
			System.out.print(" ");
			System.out.print(rect.y);
			System.out.print(" ");
			System.out.print(rect.width);
			System.out.print(" ");
			System.out.print(rect.height);
			System.out.print(")\n");
		}
		//for windows only barcode.close();
		return res;
  }
}
