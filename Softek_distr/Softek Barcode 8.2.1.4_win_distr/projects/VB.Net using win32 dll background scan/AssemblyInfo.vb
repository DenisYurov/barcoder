Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("SoftekBarcodeLib")> 
<Assembly: AssemblyDescription("Softek Barcode Managed Component")> 
<Assembly: AssemblyCompany("Softek Software")> 
<Assembly: AssemblyProduct("Softek Barcode Toolkit for Windows")> 
<Assembly: AssemblyCopyright("Softek Software Ltd 2002 - 2006")> 
<Assembly: AssemblyTrademark("Softek Software Ltd")> 
<Assembly: CLSCompliant(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("799C5998-262E-47C5-A131-16D74FCFBA27")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("7.0.1.1")> 
