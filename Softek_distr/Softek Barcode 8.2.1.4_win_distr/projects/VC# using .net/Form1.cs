using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Runtime.InteropServices;

namespace BarcodeReader
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
    {
        [DllImport("kernel32")]
        public extern static int LoadLibrary(string librayName);

		internal System.Windows.Forms.OpenFileDialog OpenFileDialog1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
        internal TextBox Result;
        internal Button ImageBtn;
        internal Button ReadBarcode;
        internal TextBox FilePath;
        private Label label1;
        internal CheckBox CheckBitmap;
        internal Button buttonClose;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.OpenFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.Result = new System.Windows.Forms.TextBox();
            this.ImageBtn = new System.Windows.Forms.Button();
            this.ReadBarcode = new System.Windows.Forms.Button();
            this.FilePath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.CheckBitmap = new System.Windows.Forms.CheckBox();
            this.buttonClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // OpenFileDialog1
            // 
            this.OpenFileDialog1.DefaultExt = "tif";
            this.OpenFileDialog1.Filter = "Images (*.tif,*.pdf,*.bmp,*.jpg)|*.tif;*.pdf;*.jpg;*.tiff;*.bmp;*.jpeg";
            this.OpenFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.OpenFileDialog1_FileOk);
            // 
            // Result
            // 
            this.Result.Location = new System.Drawing.Point(12, 40);
            this.Result.Multiline = true;
            this.Result.Name = "Result";
            this.Result.ReadOnly = true;
            this.Result.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.Result.Size = new System.Drawing.Size(608, 238);
            this.Result.TabIndex = 3;
            this.Result.TextChanged += new System.EventHandler(this.Result_TextChanged);
            // 
            // ImageBtn
            // 
            this.ImageBtn.Location = new System.Drawing.Point(518, 12);
            this.ImageBtn.Name = "ImageBtn";
            this.ImageBtn.Size = new System.Drawing.Size(102, 23);
            this.ImageBtn.TabIndex = 1;
            this.ImageBtn.Text = "Browse";
            this.ImageBtn.Click += new System.EventHandler(this.ImageBtn_Click);
            // 
            // ReadBarcode
            // 
            this.ReadBarcode.Location = new System.Drawing.Point(518, 303);
            this.ReadBarcode.Name = "ReadBarcode";
            this.ReadBarcode.Size = new System.Drawing.Size(102, 24);
            this.ReadBarcode.TabIndex = 2;
            this.ReadBarcode.Text = "Read";
            this.ReadBarcode.Click += new System.EventHandler(this.ReadBarcode_Click);
            // 
            // FilePath
            // 
            this.FilePath.Location = new System.Drawing.Point(66, 14);
            this.FilePath.Name = "FilePath";
            this.FilePath.ReadOnly = true;
            this.FilePath.Size = new System.Drawing.Size(446, 20);
            this.FilePath.TabIndex = 0;
            this.FilePath.TextChanged += new System.EventHandler(this.FilePath_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "File";
            // 
            // CheckBitmap
            // 
            this.CheckBitmap.AutoSize = true;
            this.CheckBitmap.Location = new System.Drawing.Point(12, 284);
            this.CheckBitmap.Name = "CheckBitmap";
            this.CheckBitmap.Size = new System.Drawing.Size(389, 17);
            this.CheckBitmap.TabIndex = 10;
            this.CheckBitmap.Text = "Load page 1 of image into a Bitmap object and call ScanBarCodeFromBitmap";
            this.CheckBitmap.UseVisualStyleBackColor = true;
            // 
            // buttonClose
            // 
            this.buttonClose.Location = new System.Drawing.Point(410, 303);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(102, 24);
            this.buttonClose.TabIndex = 11;
            this.buttonClose.Text = "Close";
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(632, 338);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.CheckBitmap);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Result);
            this.Controls.Add(this.ReadBarcode);
            this.Controls.Add(this.FilePath);
            this.Controls.Add(this.ImageBtn);
            this.Name = "Form1";
            this.Text = "Barcode Reader C#";
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void ImageBtn_Click(object sender, System.EventArgs e)
		{
			OpenFileDialog1.ShowDialog() ;
			FilePath.Text = OpenFileDialog1.FileName ;
		}


        private void ReadBarcode_Click(object sender, System.EventArgs e)
        {
		    SoftekBarcodeNet.BarcodeReader barcode ;

            Result.Text = "Using .Net component Class Interface to read barcodes\r\n";

            // The SoftekBarcodeNet component requires the following 2 DLL files, which have been included as items in this project (with copy to output directory enabled):
            // SoftekBarcodeDLL.dll for 32-bit versions of windows
            // SoftekBarcode64DLL.dll for 64-bit versions of windows.
            // The SoftekBarcodeNet component handles the loading of the correct DLL.
            //
            // The following 2 DLL files are also used if you are reading from PDF documents:
            // 
            // DebenuPDFLibraryDLLXXXX.dll
            // DebenuPDFLibrary64DLLXXXX.dll
            //
            // You can also create an instance of SoftekBarcodeNet using a path to the location of the DLL files
            // e.g barcode = new SoftekBarcodeNet.BarcodeReader("/path/to/dll/files")

            using (barcode = new SoftekBarcodeNet.BarcodeReader())
            {

                // Enter your license key here
                // You can get a trial license key from sales@bardecode.com
                // Example:
                // barcode.LicenseKey = "MY LICENSE KEY";

                // Turn on the barcode types you want to read.
                // Turn off the barcode types you don't want to read (this will increase the speed of your application)
                barcode.ReadCode128 = true;
                barcode.ReadCode39 = true;
                barcode.ReadCode25 = true;
                barcode.ReadEAN13 = true;
                barcode.ReadEAN8 = true;
                barcode.ReadUPCA = true;
                barcode.ReadUPCE = true;
                barcode.ReadCodabar = false;
                barcode.ReadPDF417 = true;
                barcode.ReadDataMatrix = true;
                barcode.ReadDatabar = true;
                barcode.ReadMicroPDF417 = false;
                barcode.ReadQRCode = true;

                // Databar Options is a mask that controls which type of databar barcodes will be read and whether or not
                // the software will look for a quiet zone around the barcode.
                // 1 = 2D-Linkage flag (handle micro-PDf417 barcodes as supplementary data - requires ReadMicroPDF417 to be true).
                // 2 = Read RSS14
                // 4 = Read RSS14 Stacked
                // 8 = Read Limited
                // 16 = Read Expanded
                // 32 = Read Expanded Stacked
                // 64 = Require quiet zone
                barcode.DatabarOptions = 255;

                // If you want to read more than one barcode then set Multiple Read to 1
                // Setting MutlipleRead to False will make the recognition faster
                barcode.MultipleRead = true;

                // Noise reduction takes longer but can make it possible to read some difficult barcodes
                // When using noise reduction a typical value is 10 - the smaller the value the more effect it has.
                // A zero value turns off noise reduction. 
                // barcode.NoiseReduction = 0 ;

                // You may need to set a small quiet zone if your barcodes are close to text and pictures in the image.
                // A value of zero uses the default.
                barcode.QuietZoneSize = 0;

                // LineJump controls the frequency at which scan lines in an image are sampled.
                // The default is 9 - decrease this for difficult barcodes.
                barcode.LineJump = 1;

                // You can restrict your search to a particular area of the image if you wish.
                // This example limits the search to the upper half of the page
                // System.Drawing.Rectangle scanArea = new System.Drawing.Rectangle(0, 0, 100, 50);
                // barcode.SetScanRect(scanArea, 1);

                // Set the direction that the barcode reader should scan for barcodes
                // The value is a mask where 1 = Left to Right, 2 = Top to Bottom, 4 = Right To Left, 8 = Bottom to Top
                barcode.ScanDirection = 15;

                // SkewTolerance controls the angle of skew that the barcode toolkit will tolerate. By default
                // the toolkit checks for barcodes along horizontal and vertical lines in an image. This works 
                // OK for most barcodes because even at an angle it is possible to pass a line through the entire
                // length. SkewTolerance can range from 0 to 5 and allows for barcodes skewed to an angle of 45
                // degrees.
                barcode.SkewTolerance = 0;

                // ColorProcessingLevel controls how much time the toolkit will searching a color image for a barcode.
                // The default value is 2 and the range of values is 0 to 5. If ColorThreshold is non-zero then 
                // ColorProcessingLevel is effectively set to 0.
                barcode.ColorProcessingLevel = 2;

                // MaxLength and MinLength can be used to specify the number of characters you expect to
                // find in a barcode. This can be useful to increase accuracy or if you wish to ignore some
                // barcodes in an image.
                barcode.MinLength = 4;
                barcode.MaxLength = 999;

                // When the toolkit scans an image it records the score it gets for each barcode that
                // MIGHT be in the image. If the scores recorded for any of the barcodes are >= PrefOccurrence
                // then only these barcodes are returned. Otherwise, any barcode whose scores are >= MinOccurrence
                // are reported. If you have a very poor quality image then try setting MinOccurrence to 1, but you
                // may find that some false positive results are returned.
                // barcode.MinOccurrence = 2 ;
                // barcode.PrefOccurrence = 4 ;

                // Flags for handling PDF files
                // PdfImageOnly defaults to true and indicates that the PDF documents are simple images.
                barcode.PdfImageOnly = true;

                // The PdfImageExtractOptions mask controls how images are removed from PDF documents (when PdfImageOnly is True)
                // 1 = Enable fast extraction
                // 2 = Auto-invert black and white images
                // 4 = Auto-merge strips
                // 8 = Auto-correct photometric values in black and white images
                barcode.PdfImageExtractOptions = 15;

                // The PdfImageRasterOptions mask controls how images are rasterized when PdfImageOnly is false or when image extraction fails
                // 1 = Use alternative pdf-to-tif conversion function
                // 2 = Always use pdf-to-tif conversion rather than loading the rasterized image directly into memory
                barcode.PdfImageRasterOptions = 0;

                // PdfDpi and PdfBpp control what sort of image the PDF document is rasterized into
                barcode.PdfDpi = 300;
                barcode.PdfBpp = 8;

                int nBarCode;

                if (CheckBitmap.Checked)
                {
                    if (FilePath.Text.EndsWith(".pdf", System.StringComparison.CurrentCultureIgnoreCase))
                    {
                        MessageBox.Show("Cannot load PDF files into bitmaps");
                        return;
                    }
                    System.Drawing.Bitmap bm = new System.Drawing.Bitmap(FilePath.Text);
                    Result.Text += "Loading image into a bitmap and calling ScanBarCodeFromBitmap\r\n";
                    nBarCode = barcode.ScanBarCodeFromBitmap(bm);
                    bm.Dispose();
                }
                else
                {
                    nBarCode = barcode.ScanBarCode(FilePath.Text);
                }

                if (nBarCode <= -6)
                {
                    Result.Text += "License key error: either an evaluation key has expired or the license key is not valid for processing pdf documents\r\n";
                }
                else if (nBarCode < 0)
                {
                    Result.Text += "ScanBarCode returned error number ";
                    Result.Text += nBarCode.ToString();
                    Result.Text += "\r\n";
                    Result.Text += "Last Softek Error Number = ";
                    Result.Text += barcode.GetLastError().ToString();
                    Result.Text += "\r\n";
                    Result.Text += "Last Windows Error Number = ";
                    Result.Text += barcode.GetLastWinError().ToString();
                    Result.Text += "\r\n";
                }
                else if (nBarCode == 0)
                {
                    Result.Text += "No barcode found on this image\r\n";
                }
                else
                {
                    Result.Text += "\r\n";
                    for (int i = 1; i <= nBarCode; i++)
                    {
                        Result.Text += String.Format("Barcode {0}\r\n", i);
                        Result.Text += String.Format("Value = {0}\r\n", barcode.GetBarString(i));
                        Result.Text += String.Format("Type = {0}\r\n", barcode.GetBarStringType(i));
                        Result.Text += String.Format("Quality Score = {0}/5\r\n", barcode.GetBarStringQualityScore(i));
                        int nDirection = barcode.GetBarStringDirection(i);
                        if (nDirection == 1)
                            Result.Text += "Direction = Left to Right\r\n";
                        else if (nDirection == 2)
                            Result.Text += "Direction = Top to Bottom\r\n";
                        else if (nDirection == 4)
                            Result.Text += "Direction = Right to Left\r\n";
                        else if (nDirection == 8)
                            Result.Text += "Direction = Bottom to Top\r\n";
                        int nPage = barcode.GetBarStringPage(i);
                        Result.Text += String.Format("Page = {0}\r\n", nPage);
                        System.Drawing.Rectangle rect = barcode.GetBarStringRect(i);
                        Result.Text += String.Format("Top Left = ({0},{1})\r\n", rect.X, rect.Y);
                        Result.Text += String.Format("Bottom Right = ({0},{1})\r\n", rect.X + rect.Width, rect.Y + rect.Height);
                        Result.Text += "\r\n";
                    }
                }
            }

		}

		private void PictureBox1_Click(object sender, System.EventArgs e)
		{
		
		}

        private void ResultFromPicture_TextChanged(object sender, EventArgs e)
        {

        }

        private void Label3_Click(object sender, EventArgs e)
        {

        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }

        private void Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void FilePath_TextChanged(object sender, EventArgs e)
        {

        }

        private void Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Result_TextChanged(object sender, EventArgs e)
        {

        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void OpenFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
	}
}
