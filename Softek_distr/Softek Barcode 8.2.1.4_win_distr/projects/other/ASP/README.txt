Before using the ASP example please modify the code to set a valid license key at the line:

barcode.LicenseKey = "MY LICENSE KEY"

Temporary license keys can be obtained from sales@bardecode.com

If you do not do this then the ASP example will hang your web server, which will then need restarting.
