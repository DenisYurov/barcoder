; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CSampleBarcodeReaderDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "SampleBarcodeReader.h"

ClassCount=3
Class1=CSampleBarcodeReaderApp
Class2=CSampleBarcodeReaderDlg
Class3=CAboutDlg

ResourceCount=5
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_SAMPLEBARCODEREADER_DIALOG
Resource4=IDD_ABOUTBOX (English (U.S.))
Resource5=IDD_SAMPLEBARCODEREADER_DIALOG (English (U.S.))

[CLS:CSampleBarcodeReaderApp]
Type=0
HeaderFile=SampleBarcodeReader.h
ImplementationFile=SampleBarcodeReader.cpp
Filter=N

[CLS:CSampleBarcodeReaderDlg]
Type=0
HeaderFile=SampleBarcodeReaderDlg.h
ImplementationFile=SampleBarcodeReaderDlg.cpp
Filter=D
LastObject=IDC_RESULTS
BaseClass=CDialog
VirtualFilter=dWC

[CLS:CAboutDlg]
Type=0
HeaderFile=SampleBarcodeReaderDlg.h
ImplementationFile=SampleBarcodeReaderDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889
Class=CAboutDlg


[DLG:IDD_SAMPLEBARCODEREADER_DIALOG]
Type=1
ControlCount=3
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Class=CSampleBarcodeReaderDlg

[DLG:IDD_SAMPLEBARCODEREADER_DIALOG (English (U.S.))]
Type=1
Class=CSampleBarcodeReaderDlg
ControlCount=10
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_FILE,edit,1350633600
Control4=IDC_BROWSE,button,1342242816
Control5=IDC_STATIC,static,1342308352
Control6=IDC_RADIO_DLL,button,1342177289
Control7=IDC_RADIO_OCX,button,1342177289
Control8=IDC_RESULTS,edit,1352730756
Control9=IDC_STATIC,static,1342308352
Control10=IDC_SOFTEKBARCODECTRL1,{D57637EC-D58F-42A8-8D1D-E890E8645813},1342242816

[DLG:IDD_ABOUTBOX (English (U.S.))]
Type=1
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

