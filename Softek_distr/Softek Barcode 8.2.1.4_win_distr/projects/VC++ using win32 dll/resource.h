//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SampleBarcodeReader.rc
//
#define IDOK2                           3
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_SAMPLEBARCODEREADER_DIALOG  102
#define IDR_MAINFRAME                   128
#define IDC_FILE                        1000
#define IDC_BROWSE                      1001
#define IDC_RADIO_DLL                   1003
#define IDC_RADIO_OCX                   1004
#define IDC_RADIO_COM                   1005
#define IDC_RESULTS                     1006
#define IDC_SOFTEKBARCODECTRL1          1007
#define IDC_PROGRESS1                   1008
#define IDC_CHECK_BACKGROUND            1009

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1010
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
