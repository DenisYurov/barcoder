// Machine generated IDispatch wrapper class(es) created by Microsoft Visual C++

// NOTE: Do not modify the contents of this file.  If this class is regenerated by
//  Microsoft Visual C++, your modifications will be overwritten.


#include "stdafx.h"
#include "softekbarcode.h"

/////////////////////////////////////////////////////////////////////////////
// CSoftekBarcode

IMPLEMENT_DYNCREATE(CSoftekBarcode, CWnd)

/////////////////////////////////////////////////////////////////////////////
// CSoftekBarcode properties

CString CSoftekBarcode::GetDebugTraceFile()
{
	CString result;
	GetProperty(0x1, VT_BSTR, (void*)&result);
	return result;
}

void CSoftekBarcode::SetDebugTraceFile(LPCTSTR propVal)
{
	SetProperty(0x1, VT_BSTR, propVal);
}

long CSoftekBarcode::GetLineJump()
{
	long result;
	GetProperty(0x2, VT_I4, (void*)&result);
	return result;
}

void CSoftekBarcode::SetLineJump(long propVal)
{
	SetProperty(0x2, VT_I4, propVal);
}

float CSoftekBarcode::GetLowerRatio()
{
	float result;
	GetProperty(0x3, VT_R4, (void*)&result);
	return result;
}

void CSoftekBarcode::SetLowerRatio(float propVal)
{
	SetProperty(0x3, VT_R4, propVal);
}

short CSoftekBarcode::GetMinOccurrence()
{
	short result;
	GetProperty(0x4, VT_I2, (void*)&result);
	return result;
}

void CSoftekBarcode::SetMinOccurrence(short propVal)
{
	SetProperty(0x4, VT_I2, propVal);
}

BOOL CSoftekBarcode::GetMultipleRead()
{
	BOOL result;
	GetProperty(0x5, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetMultipleRead(BOOL propVal)
{
	SetProperty(0x5, VT_BOOL, propVal);
}

short CSoftekBarcode::GetNoiseReduction()
{
	short result;
	GetProperty(0x6, VT_I2, (void*)&result);
	return result;
}

void CSoftekBarcode::SetNoiseReduction(short propVal)
{
	SetProperty(0x6, VT_I2, propVal);
}

short CSoftekBarcode::GetPageNo()
{
	short result;
	GetProperty(0x7, VT_I2, (void*)&result);
	return result;
}

void CSoftekBarcode::SetPageNo(short propVal)
{
	SetProperty(0x7, VT_I2, propVal);
}

short CSoftekBarcode::GetPrefOccurrence()
{
	short result;
	GetProperty(0x8, VT_I2, (void*)&result);
	return result;
}

void CSoftekBarcode::SetPrefOccurrence(short propVal)
{
	SetProperty(0x8, VT_I2, propVal);
}

short CSoftekBarcode::GetQuietZoneSize()
{
	short result;
	GetProperty(0x9, VT_I2, (void*)&result);
	return result;
}

void CSoftekBarcode::SetQuietZoneSize(short propVal)
{
	SetProperty(0x9, VT_I2, propVal);
}

short CSoftekBarcode::GetRotation()
{
	short result;
	GetProperty(0xa, VT_I2, (void*)&result);
	return result;
}

void CSoftekBarcode::SetRotation(short propVal)
{
	SetProperty(0xa, VT_I2, propVal);
}

float CSoftekBarcode::GetUpperRatio()
{
	float result;
	GetProperty(0xb, VT_R4, (void*)&result);
	return result;
}

void CSoftekBarcode::SetUpperRatio(float propVal)
{
	SetProperty(0xb, VT_R4, propVal);
}

BOOL CSoftekBarcode::GetReadCode39()
{
	BOOL result;
	GetProperty(0xc, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetReadCode39(BOOL propVal)
{
	SetProperty(0xc, VT_BOOL, propVal);
}

BOOL CSoftekBarcode::GetReadEAN13()
{
	BOOL result;
	GetProperty(0xd, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetReadEAN13(BOOL propVal)
{
	SetProperty(0xd, VT_BOOL, propVal);
}

BOOL CSoftekBarcode::GetReadEAN8()
{
	BOOL result;
	GetProperty(0xe, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetReadEAN8(BOOL propVal)
{
	SetProperty(0xe, VT_BOOL, propVal);
}

BOOL CSoftekBarcode::GetReadUPCA()
{
	BOOL result;
	GetProperty(0xf, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetReadUPCA(BOOL propVal)
{
	SetProperty(0xf, VT_BOOL, propVal);
}

BOOL CSoftekBarcode::GetReadUPCE()
{
	BOOL result;
	GetProperty(0x10, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetReadUPCE(BOOL propVal)
{
	SetProperty(0x10, VT_BOOL, propVal);
}

BOOL CSoftekBarcode::GetShowCheckDigit()
{
	BOOL result;
	GetProperty(0x11, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetShowCheckDigit(BOOL propVal)
{
	SetProperty(0x11, VT_BOOL, propVal);
}

BOOL CSoftekBarcode::GetReadCode128()
{
	BOOL result;
	GetProperty(0x12, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetReadCode128(BOOL propVal)
{
	SetProperty(0x12, VT_BOOL, propVal);
}

BOOL CSoftekBarcode::GetCode39NeedStartStop()
{
	BOOL result;
	GetProperty(0x13, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetCode39NeedStartStop(BOOL propVal)
{
	SetProperty(0x13, VT_BOOL, propVal);
}

BOOL CSoftekBarcode::GetReadCode25()
{
	BOOL result;
	GetProperty(0x14, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetReadCode25(BOOL propVal)
{
	SetProperty(0x14, VT_BOOL, propVal);
}

short CSoftekBarcode::GetColorThreshold()
{
	short result;
	GetProperty(0x15, VT_I2, (void*)&result);
	return result;
}

void CSoftekBarcode::SetColorThreshold(short propVal)
{
	SetProperty(0x15, VT_I2, propVal);
}

BOOL CSoftekBarcode::GetReadPatchCodes()
{
	BOOL result;
	GetProperty(0x16, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetReadPatchCodes(BOOL propVal)
{
	SetProperty(0x16, VT_BOOL, propVal);
}

short CSoftekBarcode::GetScanDirection()
{
	short result;
	GetProperty(0x17, VT_I2, (void*)&result);
	return result;
}

void CSoftekBarcode::SetScanDirection(short propVal)
{
	SetProperty(0x17, VT_I2, propVal);
}

BOOL CSoftekBarcode::GetUseOverSampling()
{
	BOOL result;
	GetProperty(0x18, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetUseOverSampling(BOOL propVal)
{
	SetProperty(0x18, VT_BOOL, propVal);
}

short CSoftekBarcode::GetMinLength()
{
	short result;
	GetProperty(0x19, VT_I2, (void*)&result);
	return result;
}

void CSoftekBarcode::SetMinLength(short propVal)
{
	SetProperty(0x19, VT_I2, propVal);
}

short CSoftekBarcode::GetMaxLength()
{
	short result;
	GetProperty(0x1a, VT_I2, (void*)&result);
	return result;
}

void CSoftekBarcode::SetMaxLength(short propVal)
{
	SetProperty(0x1a, VT_I2, propVal);
}

short CSoftekBarcode::GetBitmapResolution()
{
	short result;
	GetProperty(0x1b, VT_I2, (void*)&result);
	return result;
}

void CSoftekBarcode::SetBitmapResolution(short propVal)
{
	SetProperty(0x1b, VT_I2, propVal);
}

CString CSoftekBarcode::GetTifSplitPath()
{
	CString result;
	GetProperty(0x1c, VT_BSTR, (void*)&result);
	return result;
}

void CSoftekBarcode::SetTifSplitPath(LPCTSTR propVal)
{
	SetProperty(0x1c, VT_BSTR, propVal);
}

short CSoftekBarcode::GetTifSplitMode()
{
	short result;
	GetProperty(0x1d, VT_I2, (void*)&result);
	return result;
}

void CSoftekBarcode::SetTifSplitMode(short propVal)
{
	SetProperty(0x1d, VT_I2, propVal);
}

BOOL CSoftekBarcode::GetReadCodabar()
{
	BOOL result;
	GetProperty(0x1e, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetReadCodabar(BOOL propVal)
{
	SetProperty(0x1e, VT_BOOL, propVal);
}

BOOL CSoftekBarcode::GetConvertUPCEToEAN13()
{
	BOOL result;
	GetProperty(0x1f, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetConvertUPCEToEAN13(BOOL propVal)
{
	SetProperty(0x1f, VT_BOOL, propVal);
}

short CSoftekBarcode::GetSkewTolerance()
{
	short result;
	GetProperty(0x20, VT_I2, (void*)&result);
	return result;
}

void CSoftekBarcode::SetSkewTolerance(short propVal)
{
	SetProperty(0x20, VT_I2, propVal);
}

BOOL CSoftekBarcode::GetReadCode25ni()
{
	BOOL result;
	GetProperty(0x21, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetReadCode25ni(BOOL propVal)
{
	SetProperty(0x21, VT_BOOL, propVal);
}

BOOL CSoftekBarcode::GetPhotometric()
{
	BOOL result;
	GetProperty(0x22, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetPhotometric(BOOL propVal)
{
	SetProperty(0x22, VT_BOOL, propVal);
}

CString CSoftekBarcode::GetPDFTool()
{
	CString result;
	GetProperty(0x23, VT_BSTR, (void*)&result);
	return result;
}

void CSoftekBarcode::SetPDFTool(LPCTSTR propVal)
{
	SetProperty(0x23, VT_BSTR, propVal);
}

BOOL CSoftekBarcode::GetAllowDuplicateValues()
{
	BOOL result;
	GetProperty(0x24, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetAllowDuplicateValues(BOOL propVal)
{
	SetProperty(0x24, VT_BOOL, propVal);
}

short CSoftekBarcode::GetMinSeparation()
{
	short result;
	GetProperty(0x25, VT_I2, (void*)&result);
	return result;
}

void CSoftekBarcode::SetMinSeparation(short propVal)
{
	SetProperty(0x25, VT_I2, propVal);
}

BOOL CSoftekBarcode::GetCode39Checksum()
{
	BOOL result;
	GetProperty(0x26, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetCode39Checksum(BOOL propVal)
{
	SetProperty(0x26, VT_BOOL, propVal);
}

BOOL CSoftekBarcode::GetExtendedCode39()
{
	BOOL result;
	GetProperty(0x27, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetExtendedCode39(BOOL propVal)
{
	SetProperty(0x27, VT_BOOL, propVal);
}

BOOL CSoftekBarcode::GetErrorCorrection()
{
	BOOL result;
	GetProperty(0x28, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetErrorCorrection(BOOL propVal)
{
	SetProperty(0x28, VT_BOOL, propVal);
}

BOOL CSoftekBarcode::GetReadNumeric()
{
	BOOL result;
	GetProperty(0x29, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetReadNumeric(BOOL propVal)
{
	SetProperty(0x29, VT_BOOL, propVal);
}

short CSoftekBarcode::GetDespeckle()
{
	short result;
	GetProperty(0x2a, VT_I2, (void*)&result);
	return result;
}

void CSoftekBarcode::SetDespeckle(short propVal)
{
	SetProperty(0x2a, VT_I2, propVal);
}

CString CSoftekBarcode::GetPattern()
{
	CString result;
	GetProperty(0x2b, VT_BSTR, (void*)&result);
	return result;
}

void CSoftekBarcode::SetPattern(LPCTSTR propVal)
{
	SetProperty(0x2b, VT_BSTR, propVal);
}

short CSoftekBarcode::GetPdfBpp()
{
	short result;
	GetProperty(0x2c, VT_I2, (void*)&result);
	return result;
}

void CSoftekBarcode::SetPdfBpp(short propVal)
{
	SetProperty(0x2c, VT_I2, propVal);
}

short CSoftekBarcode::GetPdfDpi()
{
	short result;
	GetProperty(0x2d, VT_I2, (void*)&result);
	return result;
}

void CSoftekBarcode::SetPdfDpi(short propVal)
{
	SetProperty(0x2d, VT_I2, propVal);
}

short CSoftekBarcode::GetMinSpaceBarWidth()
{
	short result;
	GetProperty(0x2e, VT_I2, (void*)&result);
	return result;
}

void CSoftekBarcode::SetMinSpaceBarWidth(short propVal)
{
	SetProperty(0x2e, VT_I2, propVal);
}

BOOL CSoftekBarcode::GetReadPDF417()
{
	BOOL result;
	GetProperty(0x2f, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetReadPDF417(BOOL propVal)
{
	SetProperty(0x2f, VT_BOOL, propVal);
}

BOOL CSoftekBarcode::GetMedianFilter()
{
	BOOL result;
	GetProperty(0x30, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetMedianFilter(BOOL propVal)
{
	SetProperty(0x30, VT_BOOL, propVal);
}

BOOL CSoftekBarcode::GetCode25Checksum()
{
	BOOL result;
	GetProperty(0x31, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetCode25Checksum(BOOL propVal)
{
	SetProperty(0x31, VT_BOOL, propVal);
}

short CSoftekBarcode::GetEncoding()
{
	short result;
	GetProperty(0x32, VT_I2, (void*)&result);
	return result;
}

void CSoftekBarcode::SetEncoding(short propVal)
{
	SetProperty(0x32, VT_I2, propVal);
}

short CSoftekBarcode::GetGammaCorrection()
{
	short result;
	GetProperty(0x33, VT_I2, (void*)&result);
	return result;
}

void CSoftekBarcode::SetGammaCorrection(short propVal)
{
	SetProperty(0x33, VT_I2, propVal);
}

short CSoftekBarcode::GetSkewLineJump()
{
	short result;
	GetProperty(0x34, VT_I2, (void*)&result);
	return result;
}

void CSoftekBarcode::SetSkewLineJump(short propVal)
{
	SetProperty(0x34, VT_I2, propVal);
}

BOOL CSoftekBarcode::GetReadMicroPDF417()
{
	BOOL result;
	GetProperty(0x35, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetReadMicroPDF417(BOOL propVal)
{
	SetProperty(0x35, VT_BOOL, propVal);
}

BOOL CSoftekBarcode::GetReadShortCode128()
{
	BOOL result;
	GetProperty(0x36, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetReadShortCode128(BOOL propVal)
{
	SetProperty(0x36, VT_BOOL, propVal);
}

short CSoftekBarcode::GetShortCode128MinLength()
{
	short result;
	GetProperty(0x37, VT_I2, (void*)&result);
	return result;
}

void CSoftekBarcode::SetShortCode128MinLength(short propVal)
{
	SetProperty(0x37, VT_I2, propVal);
}

BOOL CSoftekBarcode::GetReadDataMatrix()
{
	BOOL result;
	GetProperty(0x38, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetReadDataMatrix(BOOL propVal)
{
	SetProperty(0x38, VT_BOOL, propVal);
}

BOOL CSoftekBarcode::GetReadDatabar()
{
	BOOL result;
	GetProperty(0x39, VT_BOOL, (void*)&result);
	return result;
}

void CSoftekBarcode::SetReadDatabar(BOOL propVal)
{
	SetProperty(0x39, VT_BOOL, propVal);
}

short CSoftekBarcode::GetColorProcessingLevel()
{
	short result;
	GetProperty(0x3a, VT_I2, (void*)&result);
	return result;
}

void CSoftekBarcode::SetColorProcessingLevel(short propVal)
{
	SetProperty(0x3a, VT_I2, propVal);
}

short CSoftekBarcode::GetDatabarOptions()
{
	short result;
	GetProperty(0x3b, VT_I2, (void*)&result);
	return result;
}

void CSoftekBarcode::SetDatabarOptions(short propVal)
{
	SetProperty(0x3b, VT_I2, propVal);
}

CString CSoftekBarcode::GetLicenseKey()
{
	CString result;
	GetProperty(0x3c, VT_BSTR, (void*)&result);
	return result;
}

void CSoftekBarcode::SetLicenseKey(LPCTSTR propVal)
{
	SetProperty(0x3c, VT_BSTR, propVal);
}

/////////////////////////////////////////////////////////////////////////////
// CSoftekBarcode operations

CString CSoftekBarcode::GetBarString(short index)
{
	CString result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x3d, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		index);
	return result;
}

short CSoftekBarcode::ScanBarCode(LPCTSTR file)
{
	short result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x3e, DISPATCH_METHOD, VT_I2, (void*)&result, parms,
		file);
	return result;
}

short CSoftekBarcode::ScanBarCodeFromBitmap(long hBitmap)
{
	short result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x3f, DISPATCH_METHOD, VT_I2, (void*)&result, parms,
		hBitmap);
	return result;
}

CString CSoftekBarcode::GetBarStringType(short index)
{
	CString result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x40, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		index);
	return result;
}

short CSoftekBarcode::GetBarStringPos(short nBarCode, long* pTopLeftX, long* pTopLeftY, long* pBotRightX, long* pBotRightY)
{
	short result;
	static BYTE parms[] =
		VTS_I2 VTS_PI4 VTS_PI4 VTS_PI4 VTS_PI4;
	InvokeHelper(0x41, DISPATCH_METHOD, VT_I2, (void*)&result, parms,
		nBarCode, pTopLeftX, pTopLeftY, pBotRightX, pBotRightY);
	return result;
}

void CSoftekBarcode::SaveBitmap(long hBitmap, LPCTSTR strFilePath)
{
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x42, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 hBitmap, strFilePath);
}

short CSoftekBarcode::GetBarStringDirection(short nBarCode)
{
	short result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x43, DISPATCH_METHOD, VT_I2, (void*)&result, parms,
		nBarCode);
	return result;
}

short CSoftekBarcode::ScanBarCodeFromDIB(long hDIB)
{
	short result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x44, DISPATCH_METHOD, VT_I2, (void*)&result, parms,
		hDIB);
	return result;
}

BOOL CSoftekBarcode::SetScanRect(long TopLeftX, long TopLeftY, long BottomRightX, long BottomRightY, short MappingMode)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I2;
	InvokeHelper(0x45, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		TopLeftX, TopLeftY, BottomRightX, BottomRightY, MappingMode);
	return result;
}

BOOL CSoftekBarcode::SaveResults(LPCTSTR strFilePath)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x46, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		strFilePath);
	return result;
}

BOOL CSoftekBarcode::LoadXMLSettings(LPCTSTR strFilePath, BOOL silent)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BOOL;
	InvokeHelper(0x47, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		strFilePath, silent);
	return result;
}

BOOL CSoftekBarcode::ExportXMLSettings(LPCTSTR strFilePath)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x48, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		strFilePath);
	return result;
}

BOOL CSoftekBarcode::ProcessXML(LPCTSTR inputFilePath, LPCTSTR outputFilePath, BOOL silent)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BOOL;
	InvokeHelper(0x49, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		inputFilePath, outputFilePath, silent);
	return result;
}

void CSoftekBarcode::AboutBox()
{
	InvokeHelper(0xfffffdd8, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}
