

Public Class Form1
    Inherits System.Windows.Forms.Form


    ' PLATFORM is defined in project properties/Compile/Advanced Compile Options/Custom constants
#If PLATFORM = "x64" Then

    ' These declarations are only needed if you are using the DLL interface
    Private Declare Function mtCreateBarcodeInstance Lib "SoftekBarcode64DLL" () As System.IntPtr
    Private Declare Function mtDestroyBarcodeInstance Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetAllowDuplicateValues Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetBarStringDirection Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal nBarCode As Short) As Short
    Private Declare Function mtGetBarStringQualityScore Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal nBarCode As Short) As Short
    Private Declare Function mtGetBarString Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal nBarCode As Short) As System.IntPtr
    Private Declare Function mtGetBarStringPos Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal nBarCode As Short, ByRef TopLeftX As Integer, ByRef TopLeftY As Integer, ByRef BottomRightX As Integer, ByRef BottomRightY As Integer) As Short
    Private Declare Function mtGetBarStringType Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal nBarCode As Short) As System.IntPtr
    Private Declare Function mtGetBitmapResolution Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Long
    Private Declare Function mtGetCode39Checksum Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetCode39NeedStartStop Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetColorThreshold Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetColorProcessingLevel Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetConvertUPCEToEAN13 Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetDebugTraceFile Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As System.IntPtr
    Private Declare Function mtGetErrorCorrection Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetExtendedCode39 Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetLicenseKey Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As System.IntPtr
    Private Declare Function mtGetLineJump Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Integer
    Private Declare Function mtGetLowerRatio Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Double
    Private Declare Function mtGetMaxLength Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetMedianFilter Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetMinLength Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetMinOccurrence Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetMinSpaceBarWidth Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetMultipleRead Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetNoiseReduction Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Integer
    Private Declare Function mtGetPageNo Lib "SoftekBarcode64DLL" (ByVal hBarcode As String) As Short
    Private Declare Function mtGetPattern Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As System.IntPtr
    Private Declare Function mtGetPdfBpp Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Integer
    Private Declare Function mtGetPdfImageOnly Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetPdfImageExtractOptions Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetPdfImageRasterOptions Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetPdfDpi Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetPhotometric Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetPrefOccurrence Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetQuietZoneSize Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetReadCodabar Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadCode128 Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadCode25 Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadCode25ni Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadCode39 Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadEAN13 Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadEAN8 Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadNumeric Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadPatchCodes Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadPDF417 Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadDataMatrix Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadQRCode Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadDatabar Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetDatabarOptions Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Integer
    Private Declare Function mtGetReadUPCA Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadUPCE Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetScanDirection Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetShowCheckDigit Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetSkewTolerance Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetTifSplitMode Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetTifSplitPath Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As System.IntPtr
    Private Declare Function mtGetUpperRatio Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Double
    Private Declare Function mtGetUseOverSampling Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtScanBarCodeFromBitmap Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal hBitmap As IntPtr) As Short
    Private Declare Function mtScanBarCode Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal strFile As String) As Short
    Private Declare Function mtScanBarCodeInBackground Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal strFile As String) As Short
    Private Declare Function mtScanBarCodeWait Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal milliSeconds As Integer) As Integer
    Private Declare Function mtScanBarCodeAbort Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Integer
    Private Declare Function mtGetBarCodeCount Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Integer
    Private Declare Function mtGetScanExitCode Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Integer
    Private Declare Function mtGetProgress Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Integer
    Private Declare Function mtSetAllowDuplicateValues Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetBitmapResolution Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Long) As Long
    Private Declare Function mtSetCode39Checksum Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetCode39NeedStartStop Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetColorThreshold Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetColorProcessingLevel Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetConvertUPCEToEAN13 Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetDebugTraceFile Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal strFile As String) As System.IntPtr
    Private Declare Function mtSetErrorCorrection Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetExtendedCode39 Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetLicenseKey Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal strFile As String) As System.IntPtr
    Private Declare Function mtSetLineJump Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Integer) As Integer
    Private Declare Function mtSetLowerRatio Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Double) As Double
    Private Declare Function mtSetMaxLength Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetMedianFilter Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetMinLength Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetMinOccurrence Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetMinSpaceBarWidth Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetMultipleRead Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetNoiseReduction Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Integer) As Integer
    Private Declare Function mtSetPageNo Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetPattern Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal strFile As String) As System.IntPtr
    Private Declare Function mtSetPdfBpp Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetPdfDpi Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetPdfImageOnly Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetPdfImageExtractOptions Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetPdfImageRasterOptions Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetPhotometric Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetPrefOccurrence Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetQuietZoneSize Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetReadCodabar Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadCode128 Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadCode25 Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadCode25ni Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadCode39 Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadEAN13 Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadEAN8 Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadNumeric Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadPatchCodes Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadPDF417 Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadDataMatrix Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadQRCode Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetDatabarOptions Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Integer) As Integer
    Private Declare Function mtSetReadDatabar Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadUPCA Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadUPCE Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetScanDirection Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetScanRect Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal TopLeftX As Integer, ByVal TopLeftY As Integer, ByVal BottomRightX As Integer, ByVal BottomRightY As Integer, ByVal MappingMode As Short) As Boolean
    Private Declare Function mtSetShowCheckDigit Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetSkewTolerance Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetTifSplitMode Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetTifSplitPath Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As String) As System.IntPtr
    Private Declare Function mtSetUpperRatio Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Double) As Double
    Private Declare Function mtSetUseOverSampling Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtLoadXMLSettings Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal strFile As String, ByVal silent As Boolean) As Short
    Private Declare Function mtSaveResults Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal strOutputFile As String) As Short
    Private Declare Function mtProcessXML Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal strInputFile As String, ByVal strOutputFile As String) As Short
    Private Declare Function mtExportXMLSettings Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal strFile As String) As Short
    Private Declare Function mtSetGammaCorrection Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetDespeckle Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetSkewLineJump Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetCode25Checksum Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadMicroPDF417 Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadShortCode128 Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetShortCode128MinLength Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetEncoding Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtGetGammaCorrection Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetDespeckle Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetSkewLineJump Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetCode25Checksum Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadMicroPDF417 Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadShortCode128 Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetShortCode128MinLength Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetEncoding Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetLastError Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetLastWinError Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetUseFastScan Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtSetUseFastScan Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtGetBarcodesAtTopOfPage Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtSetBarcodesAtTopOfPage Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtGetFastScanLineJump Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtSetFastScanLineJump Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtGetMaxBarcodesPerPage Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtSetMaxBarcodesPerPage Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtGetSkewedDatamatrix Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtSetSkewedDatamatrix Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtGetTimeOut Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Integer
    Private Declare Function mtSetTimeOut Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Integer) As Integer
    Private Declare Function mtGetSkewedLinear Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtSetSkewedLinear Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtGetReportUnreadBarcodes Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtSetReportUnreadBarcodes Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtGetShowCodabarStartStop Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtSetShowCodabarStartStop Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetCode128SearchLevel Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetMedianFilterBias Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetPdf417ChannelMode Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetPdfLocking Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetQuotedPrintableCharSet Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetCode39MaxRatioPcnt Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetDataMatrixFinderGapTolerance Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetPatchCodeMinOccurrence Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetCode128DebugMode Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetDataMatrixAutoUTF8 Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetPdf417AutoUTF8 Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetPdf417Debug Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetPDF417MacroEscapeBackslash Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetCode128Lenient Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetQRCodeAutoUTF8 Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetQRCodeAutoMedianFilter Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetQrCodeReadInverted Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetRotateBy45IfNoBarcode Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetUseOldCode128Algorithm Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetUseRunCache Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetWeightLongerBarcodes Lib "SoftekBarcode64DLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
#Else


    ' These declarations are only needed if you are using the DLL interface
    Private Declare Function mtCreateBarcodeInstance Lib "SoftekBarcodeDLL" () As System.IntPtr
    Private Declare Function mtDestroyBarcodeInstance Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetAllowDuplicateValues Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetBarStringDirection Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal nBarCode As Short) As Short
    Private Declare Function mtGetBarStringQualityScore Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal nBarCode As Short) As Short
    Private Declare Function mtGetBarString Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal nBarCode As Short) As System.IntPtr
    Private Declare Function mtGetBarStringPos Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal nBarCode As Short, ByRef TopLeftX As Integer, ByRef TopLeftY As Integer, ByRef BottomRightX As Integer, ByRef BottomRightY As Integer) As Short
    Private Declare Function mtGetBarStringType Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal nBarCode As Short) As System.IntPtr
    Private Declare Function mtGetBitmapResolution Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Long
    Private Declare Function mtGetCode39Checksum Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetCode39NeedStartStop Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetColorThreshold Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetColorProcessingLevel Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetConvertUPCEToEAN13 Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetDebugTraceFile Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As System.IntPtr
    Private Declare Function mtGetErrorCorrection Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetExtendedCode39 Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetLicenseKey Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As System.IntPtr
    Private Declare Function mtGetLineJump Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Integer
    Private Declare Function mtGetLowerRatio Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Double
    Private Declare Function mtGetMaxLength Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetMedianFilter Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetMinLength Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetMinOccurrence Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetMinSpaceBarWidth Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetMultipleRead Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetNoiseReduction Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Integer
    Private Declare Function mtGetPageNo Lib "SoftekBarcodeDLL" (ByVal hBarcode As String) As Short
    Private Declare Function mtGetPattern Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As System.IntPtr
    Private Declare Function mtGetPdfBpp Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Integer
    Private Declare Function mtGetPdfImageOnly Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetPdfImageExtractOptions Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetPdfImageRasterOptions Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetPdfDpi Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetPhotometric Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetPrefOccurrence Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetQuietZoneSize Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetReadCodabar Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadCode128 Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadCode25 Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadCode25ni Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadCode39 Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadEAN13 Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadEAN8 Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadNumeric Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadPatchCodes Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadPDF417 Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadDataMatrix Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadQRCode Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadDatabar Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetDatabarOptions Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Integer
    Private Declare Function mtGetReadUPCA Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadUPCE Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetScanDirection Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetShowCheckDigit Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetSkewTolerance Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetTifSplitMode Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetTifSplitPath Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As System.IntPtr
    Private Declare Function mtGetUpperRatio Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Double
    Private Declare Function mtGetUseOverSampling Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtScanBarCodeFromBitmap Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal hBitmap As IntPtr) As Short
    Private Declare Function mtScanBarCode Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal strFile As String) As Short
    Private Declare Function mtScanBarCodeInBackground Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal strFile As String) As Short
    Private Declare Function mtScanBarCodeWait Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal milliSeconds As Integer) As Integer
    Private Declare Function mtScanBarCodeAbort Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Integer
    Private Declare Function mtGetBarCodeCount Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Integer
    Private Declare Function mtGetScanExitCode Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Integer
    Private Declare Function mtGetProgress Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Integer
    Private Declare Function mtSetAllowDuplicateValues Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetBitmapResolution Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Long) As Long
    Private Declare Function mtSetCode39Checksum Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetCode39NeedStartStop Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetColorThreshold Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetColorProcessingLevel Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetConvertUPCEToEAN13 Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetDebugTraceFile Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal strFile As String) As System.IntPtr
    Private Declare Function mtSetErrorCorrection Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetExtendedCode39 Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetLicenseKey Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal strFile As String) As System.IntPtr
    Private Declare Function mtSetLineJump Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Integer) As Integer
    Private Declare Function mtSetLowerRatio Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Double) As Double
    Private Declare Function mtSetMaxLength Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetMedianFilter Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetMinLength Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetMinOccurrence Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetMinSpaceBarWidth Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetMultipleRead Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetNoiseReduction Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Integer) As Integer
    Private Declare Function mtSetPageNo Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetPattern Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal strFile As String) As System.IntPtr
    Private Declare Function mtSetPdfBpp Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetPdfDpi Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetPdfImageOnly Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetPdfImageExtractOptions Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetPdfImageRasterOptions Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetPhotometric Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetPrefOccurrence Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetQuietZoneSize Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetReadCodabar Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadCode128 Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadCode25 Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadCode25ni Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadCode39 Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadEAN13 Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadEAN8 Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadNumeric Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadPatchCodes Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadPDF417 Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadDataMatrix Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadQRCode Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetDatabarOptions Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Integer) As Integer
    Private Declare Function mtSetReadDatabar Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadUPCA Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadUPCE Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetScanDirection Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetScanRect Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal TopLeftX As Integer, ByVal TopLeftY As Integer, ByVal BottomRightX As Integer, ByVal BottomRightY As Integer, ByVal MappingMode As Short) As Boolean
    Private Declare Function mtSetShowCheckDigit Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetSkewTolerance Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetTifSplitMode Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetTifSplitPath Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As String) As System.IntPtr
    Private Declare Function mtSetUpperRatio Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Double) As Double
    Private Declare Function mtSetUseOverSampling Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtLoadXMLSettings Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal strFile As String, ByVal silent As Boolean) As Short
    Private Declare Function mtSaveResults Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal strOutputFile As String) As Short
    Private Declare Function mtProcessXML Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal strInputFile As String, ByVal strOutputFile As String) As Short
    Private Declare Function mtExportXMLSettings Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal strFile As String) As Short
    Private Declare Function mtSetGammaCorrection Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetDespeckle Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetSkewLineJump Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetCode25Checksum Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadMicroPDF417 Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetReadShortCode128 Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetShortCode128MinLength Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetEncoding Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtGetGammaCorrection Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetDespeckle Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetSkewLineJump Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetCode25Checksum Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadMicroPDF417 Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetReadShortCode128 Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtGetShortCode128MinLength Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetEncoding Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetLastError Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetLastWinError Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtGetUseFastScan Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtSetUseFastScan Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtGetBarcodesAtTopOfPage Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtSetBarcodesAtTopOfPage Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtGetFastScanLineJump Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtSetFastScanLineJump Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtGetMaxBarcodesPerPage Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtSetMaxBarcodesPerPage Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtGetSkewedDatamatrix Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtSetSkewedDatamatrix Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtGetTimeOut Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Integer
    Private Declare Function mtSetTimeOut Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Integer) As Integer
    Private Declare Function mtGetSkewedLinear Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Boolean
    Private Declare Function mtSetSkewedLinear Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtGetReportUnreadBarcodes Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtSetReportUnreadBarcodes Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtGetShowCodabarStartStop Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr) As Short
    Private Declare Function mtSetShowCodabarStartStop Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetCode128SearchLevel Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetMedianFilterBias Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetPdf417ChannelMode Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetPdfLocking Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetQuotedPrintableCharSet Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetCode39MaxRatioPcnt Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetDataMatrixFinderGapTolerance Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetPatchCodeMinOccurrence Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetCode128DebugMode Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Short) As Short
    Private Declare Function mtSetDataMatrixAutoUTF8 Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetPdf417AutoUTF8 Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetPdf417Debug Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetPDF417MacroEscapeBackslash Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetCode128Lenient Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetQRCodeAutoUTF8 Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetQRCodeAutoMedianFilter Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetQrCodeReadInverted Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetRotateBy45IfNoBarcode Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetUseOldCode128Algorithm Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetUseRunCache Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
    Private Declare Function mtSetWeightLongerBarcodes Lib "SoftekBarcodeDLL" (ByVal hBarcode As System.IntPtr, ByVal newValue As Boolean) As Boolean
#End If

    ' Required to call DeleteObject for the DLL interface
    Declare Function DeleteObject Lib "gdi32.dll" (ByVal hObject As IntPtr) As Boolean
    Declare Function LoadLibrary Lib "kernel32.dll" Alias "LoadLibraryA" (ByVal lpLibFileName As String) As IntPtr


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CheckBitmap As System.Windows.Forms.CheckBox
    Friend WithEvents ImageFile As System.Windows.Forms.TextBox
    Friend WithEvents ImageBtn As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ReadBarcode As System.Windows.Forms.Button
    Friend WithEvents Results As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.ImageFile = New System.Windows.Forms.TextBox
        Me.ImageBtn = New System.Windows.Forms.Button
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.ReadBarcode = New System.Windows.Forms.Button
        Me.Results = New System.Windows.Forms.TextBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.CheckBitmap = New System.Windows.Forms.CheckBox
        Me.SuspendLayout()
        '
        'ImageFile
        '
        Me.ImageFile.Location = New System.Drawing.Point(114, 9)
        Me.ImageFile.Name = "ImageFile"
        Me.ImageFile.ReadOnly = True
        Me.ImageFile.Size = New System.Drawing.Size(344, 20)
        Me.ImageFile.TabIndex = 0
        '
        'ImageBtn
        '
        Me.ImageBtn.Location = New System.Drawing.Point(479, 7)
        Me.ImageBtn.Name = "ImageBtn"
        Me.ImageBtn.Size = New System.Drawing.Size(102, 23)
        Me.ImageBtn.TabIndex = 1
        Me.ImageBtn.Text = "Browse"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.DefaultExt = "tif"
        Me.OpenFileDialog1.Filter = "Images (*.tif,*.pdf,*.bmp,*.jpg)|*.tif;*.jpg;*.tiff;*.pdf;*.bmp;*.jpeg"
        '
        'ReadBarcode
        '
        Me.ReadBarcode.Location = New System.Drawing.Point(479, 303)
        Me.ReadBarcode.Name = "ReadBarcode"
        Me.ReadBarcode.Size = New System.Drawing.Size(102, 24)
        Me.ReadBarcode.TabIndex = 2
        Me.ReadBarcode.Text = "Read"
        '
        'Results
        '
        Me.Results.Location = New System.Drawing.Point(12, 56)
        Me.Results.Multiline = True
        Me.Results.Name = "Results"
        Me.Results.ReadOnly = True
        Me.Results.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.Results.Size = New System.Drawing.Size(569, 211)
        Me.Results.TabIndex = 3
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(362, 303)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(96, 23)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Close"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(23, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "File"
        '
        'CheckBitmap
        '
        Me.CheckBitmap.AutoSize = True
        Me.CheckBitmap.Location = New System.Drawing.Point(12, 283)
        Me.CheckBitmap.Name = "CheckBitmap"
        Me.CheckBitmap.Size = New System.Drawing.Size(389, 17)
        Me.CheckBitmap.TabIndex = 9
        Me.CheckBitmap.Text = "Load page 1 of image into a Bitmap object and call ScanBarCodeFromBitmap"
        Me.CheckBitmap.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(593, 338)
        Me.Controls.Add(Me.CheckBitmap)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Results)
        Me.Controls.Add(Me.ImageFile)
        Me.Controls.Add(Me.ReadBarcode)
        Me.Controls.Add(Me.ImageBtn)
        Me.Name = "Form1"
        Me.Text = "Barcode Reader"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub ImageBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImageBtn.Click
        OpenFileDialog1.ShowDialog()
        ImageFile.Text = OpenFileDialog1.FileName
    End Sub

    Private Sub ReadDLL()
        Dim nBarCodes As Integer
        Dim Path As String
        Dim i As Integer
        Dim nDirection As Integer
        Dim TopLeftX As Long
        Dim TopLeftY As Long
        Dim BottomRightX As Long
        Dim BottomRightY As Long
        Dim nPage As Integer
        Dim hBarcode As System.IntPtr

        Results.Text = ""

        hBarcode = mtCreateBarcodeInstance()

        ' Enter your license key here
        ' You can get a trial license key from sales@bardecode.com
        ' Example:
        ' mtSetLicenseKey(hBarcode, "MY LICENSE KEY")

        ' Turn on the barcode types you want to read.
        ' Turn off the barcode types you don't want to read (this will increase the speed of your application)
        mtSetReadCode128(hBarcode, True)
        mtSetReadCode39(hBarcode, True)
        mtSetReadCode25(hBarcode, True)
        mtSetReadEAN13(hBarcode, True)
        mtSetReadEAN8(hBarcode, True)
        mtSetReadUPCA(hBarcode, True)
        mtSetReadUPCE(hBarcode, True)
        mtSetReadCodabar(hBarcode, False)
        mtSetReadPDF417(hBarcode, True)
        mtSetReadDataMatrix(hBarcode, True)
        mtSetReadDatabar(hBarcode, True)
        mtSetReadMicroPDF417(hBarcode, False)
        mtSetReadQRCode(hBarcode, True)

        ' Databar Options is a mask that controls which type of databar barcodes will be read and whether or not
        ' the software will look for a quiet zone around the barcode.
        ' 1 = 2D-Linkage flag (handle micro-PDf417 barcodes as supplementary data - requires ReadMicroPDF417 to be true).
        ' 2 = Read RSS14
        ' 4 = Read RSS14 Stacked
        ' 8 = Read Limited
        ' 16 = Read Expanded
        ' 32 = Read Expanded Stacked
        ' 64 = Require quiet zone
        mtSetDatabarOptions(hBarcode, 255)

        ' If you want to read more than one barcode then set Multiple Read to 1
        ' Setting MutlipleRead to False will make the recognition faster
        mtSetMultipleRead(hBarcode, True)

        ' If you know the max number of barcodes for a single page then increase speed by setting MaxBarcodesPerPage
        mtSetMaxBarcodesPerPage(hBarcode, 0)

        ' In certain conditions (MultipleRead = false or MaxBarcodesPerPage = 1) the SDK can make fast scan of an image before performing the normal scan. This is useful if only 1 bar code is expected per page.
        mtSetUseFastScan(hBarcode, True)

        ' Noise reduction takes longer but can make it possible to read some difficult barcodes
        ' When using noise reduction a typical value is 10 - the smaller the value the more effect it has.
        ' A zero value turns off noise reduction.
        ' If you use NoiseReduction then the ScanDirection mask must be either only horizontal or only
        ' vertical (i.e 1, 2, 4, 8, 5 or 10).
        ' mtSetNoiseReduction(hBarcode, 0)

        ' You may need to set a small quiet zone if your barcodes are close to text and pictures in the image.
        ' A value of zero uses the default.
        mtSetQuietZoneSize(hBarcode, 0)

        ' LineJump controls the frequency at which scan lines in an image are sampled.
        ' The default is 1.
        mtSetLineJump(hBarcode, 1)

        ' You can restrict your search to a particular area of the image if you wish.
        ' retval = stSetScanRect(TopLeftX, TopLeftY, BottomRightX, BottomRightY, 0)

        ' Set the direction that the barcode reader should scan for barcodes
        ' The value is a mask where 1 = Left to Right, 2 = Top to Bottom, 4 = Right To Left, 8 = Bottom to Top
        mtSetScanDirection(hBarcode, 15)

        ' Set the page number to read from in a multi-page TIF file. The default is 0, which will make the
        ' toolkit check every page.
        ' mtSetPageNo(hBarcode, 1)

        ' SkewTolerance controls the angle of skew that the barcode toolkit will tolerate. By default
        ' the toolkit checks for barcodes along horizontal and vertical lines in an image. This works
        ' OK for most barcodes because even at an angle it is possible to pass a line through the entire
        ' length. SkewTolerance can range from 0 to 5 and allows for barcodes skewed to an angle of 45
        ' degrees.
        mtSetSkewTolerance(hBarcode, 0)

        ' If you know the max number of barcodes for a single page then increase speed by setting MaxBarcodesPerPage
        mtSetMaxBarcodesPerPage(hBarcode, 0)

        ' In certain conditions (MultipleRead = false or MaxBarcodesPerPage = 1) the SDK can make fast scan of an image before performing the normal scan. This is useful if only 1 bar code is expected per page.
        mtSetUseFastScan(hBarcode, True)

        ' ColorProcessingLevel controls how much time the toolkit will searching a color image for a barcode.
        ' The default value is 2 and the range of values is 0 to 5. If ColorThreshold is non-zero then 
        ' ColorProcessingLevel is effectively set to 0.
        mtSetColorProcessingLevel(hBarcode, 2)

        ' MaxLength and MinLength can be used to specify the number of characters you expect to
        ' find in a barcode. This can be useful to increase accuracy or if you wish to ignore some
        ' barcodes in an image.
        mtSetMinLength(hBarcode, 4)
        mtSetMaxLength(hBarcode, 999)

        ' When the toolkit scans an image it records the number of hits it gets for each barcode that
        ' MIGHT be in the image. If the hits recorded for any of the barcodes are >= PrefOccurrence
        ' then only these barcodes are returned. Otherwise, any barcode whose hits are >= MinOccurrence
        ' are reported. If you have a very poor quality image then try setting MinOccurrence to 1, but you
        ' may find that some false positive results are returned.
        'mtSetMinOccurrence (hBarcode, 2)
        'mtSetPrefOccurrence (hBarcode, 4)

        ' Read Code 39 barcodes in extended mode
        ' mtSetExtendedCode39(hBarcode, True)

        ' Barcode string is numeric
        ' mtSetReadNumeric(hBarcode, True)

        ' Set a regular expression for the barcode
        ' mtSetPattern(hBarcode, "^[A-Z]{2}[0-9]{5}$")

        ' If you are scanning at a high resolution and the spaces between bars are
        ' larger than 1 pixel then set MinSpaceBarWidth to 2 and increase your read rate.
        ' mtSetMinSpaceBarWidth(hBarcode, 2)

        ' MedianFilter is a useful way to clean up higher resolution images where the black bars contain white dots
        ' and the spaces contain black dots. It does not work if the space between bars is only 1 pixel wide.
        mtSetMedianFilter(hBarcode, False)

        ' ReportUnreadBarcodes can be used to warn of the presence of a barcode on a page that the SDK has not been able to decode.
        ' It currently has the following important limitations:
        ' 1. An unread linear barcode will only be reported if no other barcode was found in the same page.
        ' 2. The height of the area for an unread linear barcode will only cover a portion of the barcode.
        ' 3. Only 2-D barcodes that fail to error correct will be reported.
        ' 4. The barcode type and value will both be set to UNREAD for all unread barcodes.
        ' 5. The reporting of unread linear barcodes takes no account of settings for individual barcode types. For example, if ReadCode39 is True and 
        ' an image contains a single Code 39 barcode then this will be reported as an unread barcode.
        ' 6. 2-D barcodes are only reported as unread if the correct barcode types have been enabled.
        ' 7. Not all unread barcodes will be detected. 
        '
        ' The value is a mask with the following values: 1 = linear barcodes, 2 = Datamatrix, 4 = QR-Code, 8 = PDF-417
        mtSetReportUnreadBarcodes(hBarcode, 0)

        ' Time out for reading a barcode from a page in ms. Note that this does not include the time to load the page.
        ' 0 means no time out.
        mtSetTimeOut(hBarcode, 5000)

        ' Flags for handling PDF files
        ' PdfImageOnly defaults to true and indicates that the PDF documents are simple images.
        mtSetPdfImageOnly(hBarcode, True)

        ' The PdfImageExtractOptions mask controls how images are removed from PDF documents (when PdfImageOnly is True)
        ' 1 = Enable fast extraction
        ' 2 = Auto-invert black and white images
        ' 4 = Auto-merge strips
        ' 8 = Auto-correct photometric values in black and white images
        mtSetPdfImageExtractOptions(hBarcode, 15)

        ' The PdfImageRasterOptions mask controls how images are rasterized when PdfImageOnly is false or when image extraction fails
        ' 1 = Use alternative pdf-to-tif conversion function
        ' 2 = Always use pdf-to-tif conversion rather than loading the rasterized image directly into memory
        mtSetPdfImageRasterOptions(hBarcode, 0)

        ' PdfDpi and PdfBpp control what sort of image the PDF document is rasterized into
        mtSetPdfDpi(hBarcode, 300)
        mtSetPdfBpp(hBarcode, 8)

        ' Or you can load the settings from a xml format file
        ' See the manual for more details.
        ' mtLoadXMLSettings(hBarcode, "settings.xml")

        Path = ImageFile.Text


        WriteResult("Using DLL Interface to read barcodes")

        If CheckBitmap.Checked Then
            If (Path.EndsWith(".pdf", System.StringComparison.CurrentCultureIgnoreCase)) Then
                MessageBox.Show("Cannot load PDF files into bitmaps")
                Return
            End If

            Dim bm As System.Drawing.Bitmap
            WriteResult("Loading image into a bitmap and calling ScanBarCodeFromBitmap")
            bm = New Bitmap(Path)
            Dim hBitmap As IntPtr
            hBitmap = bm.GetHbitmap()
            nBarCodes = mtScanBarCodeFromBitmap(hBarcode, hBitmap)
            DeleteObject(hBitmap)
            bm.Dispose()
        Else
            nBarCodes = mtScanBarCode(hBarcode, Path)
        End If


        ' You can also save the results to an XML file
        ' stSaveXMLResults("results.xml")

        If nBarCodes <= -6 Then
            WriteResult("License key error: either an evaluation key has expired or the license key is not valid for processing pdf documents")
        ElseIf nBarCodes < 0 Then
            WriteResult("ScanBarCode returned error number " & nBarCodes)
            WriteResult("Last Softek Error Number = " & mtGetLastError(hBarcode))
            WriteResult("Last Windows Error Number = " & mtGetLastWinError(hBarcode))
        End If

        If nBarCodes = 0 Then
            WriteResult("Sorry - no barcodes were found in this image")
        End If

        For i = 1 To nBarCodes
            WriteResult("")
            WriteResult("Barcode " & i & ":")

            WriteResult("Value = " & ConvertUTF8IntPtrtoString(mtGetBarString(hBarcode, i)))

            WriteResult("Type = " & System.Runtime.InteropServices.Marshal.PtrToStringAnsi(mtGetBarStringType(hBarcode, i)))

            nDirection = mtGetBarStringDirection(hBarcode, i)
            If nDirection = 1 Then
                WriteResult("Direction = Left to Right")
            Else
                If nDirection = 2 Then
                    WriteResult("Direction = Top to Bottom")
                Else
                    If nDirection = 4 Then
                        WriteResult("Direction = Right to Left")
                    Else
                        If nDirection = 8 Then
                            WriteResult("Direction = Bottom to Top")
                        End If
                    End If
                End If
            End If

            nPage = mtGetBarStringPos(hBarcode, i, TopLeftX, TopLeftY, BottomRightX, BottomRightY)
            WriteResult("Page = " & nPage)
            WriteResult("Top Left = (" & TopLeftX & "," & TopLeftY & ")")
            WriteResult("Bottom Right = (" & BottomRightX & "," & BottomRightY & ")")

        Next i

        mtDestroyBarcodeInstance(hBarcode)

    End Sub
    Public Function ConvertUTF8IntPtrtoString(ByVal ptr As System.IntPtr) As String
        Dim l As Integer
        l = System.Runtime.InteropServices.Marshal.PtrToStringAnsi(ptr).Length
        Dim utf8data(l) As Byte
        System.Runtime.InteropServices.Marshal.Copy(ptr, utf8data, 0, l)
        Return System.Text.Encoding.UTF8.GetString(utf8data)
    End Function
    Private Sub ReadBarcode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReadBarcode.Click
#If PLATFORM <> "x64" Then
        If (IntPtr.Size = 8) Then
            MessageBox.Show("Please select the x64 configuation and run again")
            Return
        End If
#End If
        ReadDLL()
    End Sub
    Sub WriteResult(ByVal Result As String)
        With Results
            Results.Text += Result
            Results.Text += vbNewLine
        End With
    End Sub

    Private Sub Label2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub FilePath_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImageFile.TextChanged

    End Sub

    Private Sub Label3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Result_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Results.TextChanged

    End Sub

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Panel2_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs)

    End Sub

    Private Sub ResultFromPicture_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs)

    End Sub

    Private Sub OpenFileDialog1_FileOk(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles OpenFileDialog1.FileOk

    End Sub

    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        End
    End Sub
End Class


