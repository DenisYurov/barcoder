Public Class Form1
    Inherits System.Windows.Forms.Form

    Declare Function DeleteObject Lib "gdi32.dll" (ByVal hObject As IntPtr) As Boolean
    Declare Function LoadLibrary Lib "kernel32.dll" Alias "LoadLibraryA" (ByVal lpLibFileName As String) As IntPtr




#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CheckBitmap As System.Windows.Forms.CheckBox
    Friend WithEvents ImageFile As System.Windows.Forms.TextBox
    Friend WithEvents ImageBtn As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ReadBarcode As System.Windows.Forms.Button
    Friend WithEvents Results As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.ImageFile = New System.Windows.Forms.TextBox
        Me.ImageBtn = New System.Windows.Forms.Button
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.ReadBarcode = New System.Windows.Forms.Button
        Me.Results = New System.Windows.Forms.TextBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.CheckBitmap = New System.Windows.Forms.CheckBox
        Me.SuspendLayout()
        '
        'ImageFile
        '
        Me.ImageFile.Location = New System.Drawing.Point(114, 9)
        Me.ImageFile.Name = "ImageFile"
        Me.ImageFile.ReadOnly = True
        Me.ImageFile.Size = New System.Drawing.Size(344, 20)
        Me.ImageFile.TabIndex = 0
        '
        'ImageBtn
        '
        Me.ImageBtn.Location = New System.Drawing.Point(479, 7)
        Me.ImageBtn.Name = "ImageBtn"
        Me.ImageBtn.Size = New System.Drawing.Size(102, 23)
        Me.ImageBtn.TabIndex = 1
        Me.ImageBtn.Text = "Browse"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.DefaultExt = "tif"
        Me.OpenFileDialog1.Filter = "Images (*.tif,*.pdf,*.bmp,*.jpg)|*.tif;*.jpg;*.tiff;*.pdf;*.bmp;*.jpeg"
        '
        'ReadBarcode
        '
        Me.ReadBarcode.Location = New System.Drawing.Point(479, 303)
        Me.ReadBarcode.Name = "ReadBarcode"
        Me.ReadBarcode.Size = New System.Drawing.Size(102, 24)
        Me.ReadBarcode.TabIndex = 2
        Me.ReadBarcode.Text = "Read"
        '
        'Results
        '
        Me.Results.Location = New System.Drawing.Point(12, 56)
        Me.Results.Multiline = True
        Me.Results.Name = "Results"
        Me.Results.ReadOnly = True
        Me.Results.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.Results.Size = New System.Drawing.Size(569, 211)
        Me.Results.TabIndex = 3
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(362, 303)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(96, 23)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Close"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(23, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "File"
        '
        'CheckBitmap
        '
        Me.CheckBitmap.AutoSize = True
        Me.CheckBitmap.Location = New System.Drawing.Point(12, 283)
        Me.CheckBitmap.Name = "CheckBitmap"
        Me.CheckBitmap.Size = New System.Drawing.Size(389, 17)
        Me.CheckBitmap.TabIndex = 9
        Me.CheckBitmap.Text = "Load page 1 of image into a Bitmap object and call ScanBarCodeFromBitmap"
        Me.CheckBitmap.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(593, 338)
        Me.Controls.Add(Me.CheckBitmap)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Results)
        Me.Controls.Add(Me.ImageFile)
        Me.Controls.Add(Me.ReadBarcode)
        Me.Controls.Add(Me.ImageBtn)
        Me.Name = "Form1"
        Me.Text = "Barcode Reader"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub ImageBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImageBtn.Click
        OpenFileDialog1.ShowDialog()
        ImageFile.Text = OpenFileDialog1.FileName
    End Sub

    Private Sub ReadBarcode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReadBarcode.Click
        Dim nBarCodes As Integer
        Dim Path As String
        Dim i As Integer
        Dim nDirection As Integer
        Dim nPage As Integer
        Dim rect As System.Drawing.Rectangle
        Dim barcode As SoftekBarcodeNet.BarcodeReader

        ' The SoftekBarcodeNet component requires the following 2 DLL files, which have been included as items in this project (with copy to output directory enabled):
        ' SoftekBarcodeDLL.dll for 32-bit versions of windows
        ' SoftekBarcode64DLL.dll for 64-bit versions of windows.
        ' The SoftekBarcodeNet component handles the loading of the correct DLL.
        '
        ' The following 2 DLL files are also used if you are reading from PDF documents:
        ' 
        ' DebenuPDFLibraryDLLXXXX.dll
        ' DebenuPDFLibrary64DLLXXXX.dll
        '
        ' You can also create an instance of SoftekBarcodeNet using a path to the location of the DLL files
        ' e.g barcode = New SoftekBarcodeNet.BarcodeReader("/path/to/dll/files")

        barcode = New SoftekBarcodeNet.BarcodeReader()

        Using (barcode)
            ' Enter your license key here
            ' You can get a trial license key from sales@bardecode.com
            ' Example:
            ' barcode.LicenseKey = "MY LICENSE KEY"

            ' Turn on the barcode types you want to read.
            ' Turn off the barcode types you don't want to read (this will increase the speed of your application)
            barcode.ReadCode128 = True
            barcode.ReadCode39 = True
            barcode.ReadCode25 = True
            barcode.ReadEAN13 = True
            barcode.ReadEAN8 = True
            barcode.ReadUPCA = True
            barcode.ReadUPCE = True
            barcode.ReadCodabar = True
            barcode.ReadPDF417 = True
            barcode.ReadDataMatrix = True
            barcode.ReadDatabar = True
            barcode.ReadMicroPDF417 = False
            barcode.ReadQRCode = True

            ' Databar Options is a mask that controls which type of databar barcodes will be read and whether or not
            ' the software will look for a quiet zone around the barcode.
            ' 1 = 2D-Linkage flag (handle micro-PDf417 barcodes as supplementary data - requires ReadMicroPDF417 to be true).
            ' 2 = Read RSS14
            ' 4 = Read RSS14 Stacked
            ' 8 = Read Limited
            ' 16 = Read Expanded
            ' 32 = Read Expanded Stacked
            ' 64 = Require quiet zone
            barcode.DatabarOptions = 255

            ' If you want to read more than one barcode then set Multiple Read to 1
            ' Setting MutlipleRead to False will make the recognition faster
            barcode.MultipleRead = True

            ' Noise reduction takes longer but can make it possible to read some difficult barcodes
            ' When using noise reduction a typical value is 10 - the smaller the value the more effect it has.
            ' A zero value turns off noise reduction.
            ' barcode.NoiseReduction = 0

            ' You may need to set a small quiet zone if your barcodes are close to text and pictures in the image.
            ' A value of zero uses the default.
            barcode.QuietZoneSize = 0

            ' LineJump controls the frequency at which scan lines in an image are sampled.
            ' The default is 1.
            barcode.LineJump = 1

            ' You can restrict your search to a particular area of the image if you wish.
            ' This example limits the search to the upper half of the page
            ' Dim scanArea As System.Drawing.Rectangle
            ' scanArea = New System.Drawing.Rectangle(0, 0, 100, 50)
            ' barcode.SetScanRect(scanArea, 1)

            ' Set the direction that the barcode reader should scan for barcodes
            ' The value is a mask where 1 = Left to Right, 2 = Top to Bottom, 4 = Right To Left, 8 = Bottom to Top
            barcode.ScanDirection = 15

            ' Set the page number to read from in a multi-page TIF file. The default is 0, which will make the
            ' toolkit check every page.
            ' barcode.PageNo = 1

            ' SkewTolerance controls the angle of skew that the barcode toolkit will tolerate. By default
            ' the toolkit checks for barcodes along horizontal and vertical lines in an image. This works
            ' OK for most barcodes because even at an angle it is possible to pass a line through the entire
            ' length. SkewTolerance can range from 0 to 5 and allows for barcodes skewed to an angle of 45
            ' degrees.
            barcode.SkewTolerance = 0

            ' ColorProcessingLevel controls how much time the toolkit will searching a color image for a barcode.
            ' The default value is 2 and the range of values is 0 to 5. If ColorThreshold is non-zero then 
            ' ColorProcessingLevel is effectively set to 0.
            barcode.ColorProcessingLevel = 2

            ' MaxLength and MinLength can be used to specify the number of characters you expect to
            ' find in a barcode. This can be useful to increase accuracy or if you wish to ignore some
            ' barcodes in an image.
            barcode.MinLength = 4
            barcode.MaxLength = 999

            ' When the toolkit scans an image it records the number of hits it gets for each barcode that
            ' MIGHT be in the image. If the hits recorded for any of the barcodes are >= PrefOccurrence
            ' then only these barcodes are returned. Otherwise, any barcode whose hits are >= MinOccurrence
            ' are reported. If you have a very poor quality image then try setting MinOccurrence to 1, but you
            ' may find that some false positive results are returned.
            ' barcode.MinOccurrence = 2
            ' barcode.PrefOccurrence = 4

            ' Read Code 39 barcodes in extended mode
            ' barcode.ExtendedCode39 = True

            ' Barcode string is numeric
            ' barcode.ReadNumeric = True

            ' Set a regular expression for the barcode
            ' barcode.Pattern = "^[A-Z]{2}[0-9]{5}$"

            ' If you are scanning at a high resolution and the spaces between bars are
            ' larger than 1 pixel then set MinSpaceBarWidth to 2 and increase your read rate.
            ' barcode.MinSpaceBarWidth = 2

            ' MedianFilter is a useful way to clean up higher resolution images where the black bars contain white dots
            ' and the spaces contain black dots. It does not work if the space between bars is only 1 pixel wide.
            barcode.MedianFilter = False

            ' Flags for handling PDF files
            ' PdfImageOnly defaults to true and indicates that the PDF documents are simple images.
            barcode.PdfImageOnly = True

            ' The PdfImageExtractOptions mask controls how images are removed from PDF documents (when PdfImageOnly is True)
            ' 1 = Enable fast extraction
            ' 2 = Auto-invert black and white images
            ' 4 = Auto-merge strips
            ' 8 = Auto-correct photometric values in black and white images
            barcode.PdfImageExtractOptions = 15

            ' The PdfImageRasterOptions mask controls how images are rasterized when PdfImageOnly is false or when image extraction fails
            ' 1 = Use alternative pdf-to-tif conversion function
            ' 2 = Always use pdf-to-tif conversion rather than loading the rasterized image directly into memory
            barcode.PdfImageRasterOptions = 0

            ' PdfDpi and PdfBpp control what sort of image the PDF document is rasterized into
            barcode.PdfDpi = 300
            barcode.PdfBpp = 8

            ' Or you can load the settings from a xml format file
            ' See the manual for more details.
            ' barcode.LoadXMLSettings("settings.xml")

            Path = ImageFile.Text

            Results.Text = ""

            If CheckBitmap.Checked Then
                If (Path.EndsWith(".pdf", System.StringComparison.CurrentCultureIgnoreCase)) Then
                    MessageBox.Show("Cannot load PDF files into bitmaps")
                    Return
                End If
                Dim bm As Bitmap
                WriteResult("Loading image into a bitmap and calling ScanBarCodeFromBitmap")
                bm = New Bitmap(Path)
                nBarCodes = barcode.ScanBarCodeFromBitmap(bm)
            Else
                nBarCodes = barcode.ScanBarCode(Path)
            End If

            If nBarCodes <= -6 Then
                WriteResult("License key error: either an evaluation key has expired or the license key is not valid for processing pdf documents")
            ElseIf nBarCodes < 0 Then
                WriteResult("ScanBarCode returned error number " & nBarCodes)
                WriteResult("Last Softek Error Number = " & barcode.GetLastError())
                WriteResult("Last Windows Error Number = " & barcode.GetLastWinError())
            End If

            If nBarCodes = 0 Then
                WriteResult("Sorry - no barcodes were found in this image")
            End If

            For i = 1 To nBarCodes
                WriteResult("Barcode " & i & ":")

                WriteResult("Value = " & barcode.GetBarString(i))

                WriteResult("Type = " & barcode.GetBarStringType(i))

                WriteResult("Quality Score = " & barcode.GetBarStringQualityScore(i) & "/5")

                nDirection = barcode.GetBarStringDirection(i)
                If nDirection = 1 Then
                    WriteResult("Direction = Left to Right")
                Else
                    If nDirection = 2 Then
                        WriteResult("Direction = Top to Bottom")
                    Else
                        If nDirection = 4 Then
                            WriteResult("Direction = Right to Left")
                        Else
                            If nDirection = 8 Then
                                WriteResult("Direction = Bottom to Top")
                            End If
                        End If
                    End If
                End If

                nPage = barcode.GetBarStringPage(i)
                rect = barcode.GetBarStringRect(i)
                WriteResult("Page = " & nPage)
                WriteResult("Top Left = (" & rect.X & "," & rect.Y & ")")
                WriteResult("Bottom Right = (" & (rect.X + rect.Width) & "," & (rect.Y + rect.Height) & ")")
                WriteResult("")

            Next i

        End Using
    End Sub
    Sub WriteResult(ByVal Result As String)
        With Results
            Results.Text += Result + vbNewLine
        End With
    End Sub

    Private Sub Label2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub FilePath_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImageFile.TextChanged

    End Sub

    Private Sub Label3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Result_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Results.TextChanged

    End Sub

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Panel2_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs)

    End Sub

    Private Sub ResultFromPicture_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs)

    End Sub

    Private Sub OpenFileDialog1_FileOk(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles OpenFileDialog1.FileOk

    End Sub

    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        End
    End Sub
End Class


