# Cleans up the projects folder
find projects -type d -name debug -exec rm -rf "{}" \;
find projects -type d -name Debug -exec rm -rf "{}" \;
find projects -type d -name release -exec rm -rf "{}" \;
find projects -type d -name Release -exec rm -rf "{}" \;
find projects -type d -name obj -exec rm -rf "{}" \;
find projects -type d -name bin -exec rm -rf "{}" \;
find projects -type f -name "*BEEZER*" -exec rm -f "{}" \;
find projects -type f -name "*.ncb" -exec rm -f "{}" \;
find . -type f -name Thumbs.db -exec rm -f "{}" \;
find . -type f -name .DS_Store -exec rm -f "{}" \;


